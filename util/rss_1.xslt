<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html"/>
  <xsl:template match="/">
    <ol class="rss-items">
      <xsl:apply-templates select="//item[position() &lt; 4]"/>
    </ol>
  </xsl:template>

  <xsl:template match="item">
    <li>
      <a class="item-title">
        <xsl:attribute name="href">
          <xsl:value-of select="link"/>
        </xsl:attribute>
        <xsl:value-of select="title"/>
      </a>
      <div class="item-description">
        <xsl:value-of disable-output-escaping="yes" select="description"/>
      </div>
    </li>
  </xsl:template>
</xsl:stylesheet>
