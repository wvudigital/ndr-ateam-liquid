<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:key name="month-year" match="data[@type='item']" use="concat(Reported_Year, substring(Reported_Date, 1, 2))" />

   <xsl:template match="/data[@type='list']">
    <xsl:for-each select="data[@type='item'][count(. | key('month-year', concat(Reported_Year, substring(Reported_Date, 1, 2)))[1]) = 1]">
        <xsl:sort select="concat(Reported_Year, substring(Reported_Date, 1, 2))" order="descending" data-type="number" />
          <h2 class="accordion__heading">
              <xsl:apply-templates select="Reported_Month" mode="convertToName" />&#160;<xsl:value-of select="Reported_Year" /><br />
          </h2>
          <div class="accordion__panel">
            <ol class="incident-list" aria-label="Accordion of monthly reports">
              <xsl:for-each select="key('month-year', concat(Reported_Year, substring(Reported_Date, 1, 2)))">
              <xsl:sort select="Case_Number" order="descending" />
                <li class="incident">
                  <div class="log-date">
                    <h3 class="month-day wvu-h4"><xsl:value-of select="substring(Reported_Date, 1, 2)"/><span>/</span> <xsl:value-of select="substring(Reported_Date, 4, 2)"/><span>/</span></h3>
                    <p class="year"><xsl:value-of select="Reported_Year"/></p>
                  </div>

                  <div class="log-copy">
                    <h4 class="wvu-h4">#<xsl:value-of select="Case_Number"/>: <xsl:value-of select="Incident_code"/></h4>
                    <p><strong>Occurred: </strong><xsl:value-of select="Incident_Start_Date_Time"/></p>
                    <p><strong>Comments: </strong><xsl:value-of select="Case_Comments"/></p>
                    <p><strong>Building: </strong><xsl:value-of select="Building_Name"/></p>
                    <p><strong>Address: </strong><xsl:value-of select="Address"/></p>
                    <p><strong>Disposition: </strong><xsl:value-of select="Disposition"/></p>
                  </div>
                </li>
              </xsl:for-each>
            </ol>
          </div>
    </xsl:for-each>
   </xsl:template>

  <xsl:template match="Reported_Month" mode="convertToName">
    <!-- Make sure the month has 2 digits -->
    <xsl:variable name="twoDigitsMonth">
      <xsl:choose>
        <!-- Adds a leading zero when there's only one digit -->
        <xsl:when test="string-length(.) = 1">
          <xsl:value-of select="concat('0', .)" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="." />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- Outputs the corresponding month name -->
    <xsl:choose>
      <xsl:when test="$twoDigitsMonth='01'">January</xsl:when>
      <xsl:when test="$twoDigitsMonth='02'">February</xsl:when>
      <xsl:when test="$twoDigitsMonth='03'">March</xsl:when>
      <xsl:when test="$twoDigitsMonth='04'">April</xsl:when>
      <xsl:when test="$twoDigitsMonth='05'">May</xsl:when>
      <xsl:when test="$twoDigitsMonth='06'">June</xsl:when>
      <xsl:when test="$twoDigitsMonth='07'">July</xsl:when>
      <xsl:when test="$twoDigitsMonth='08'">August</xsl:when>
      <xsl:when test="$twoDigitsMonth='09'">September</xsl:when>
      <xsl:when test="$twoDigitsMonth='10'">October</xsl:when>
      <xsl:when test="$twoDigitsMonth='11'">November</xsl:when>
      <xsl:when test="$twoDigitsMonth='12'">December</xsl:when>

      <!-- When nothing match, outputs the month unchanged -->
      <xsl:otherwise>
        <xsl:value-of select="." />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
